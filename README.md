# Genesys Excersise

### Author: kpramesh2212@gmail.com

---
## Pre-Requisites 

The following are the pre-requisites to run this application

I have developed and tested the app with the following 

1. Operating System - Linux (preferablly ubuntu)
2. docker -  20.10.7+ 
3. docker-compose - 1.29.2+
4. Java 15 or higher (Any version of java capable of running gradle 7.2)


You don't need gradle to be installed in your system.


## How to run this application

1. Clone this repository

2. Make sure all pre-requisites are met

3. cd user-management-app

4. ./gradlew clean composeUp  (This will build and deploy the application. Wait for the deploy to complete)

5. Access the application using http://localhost:8080/swagger-ui.html

6. ./gradlew test (This should run the unit tests and integration tests)

7. The app can be un-deployed using ./gradlew composeDown

Please let me know if you have any further questions
