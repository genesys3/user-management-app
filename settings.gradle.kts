rootProject.name = "genesys-exercise"

include("user-app")
include("user-blueprints")
include("user-integration-tests")