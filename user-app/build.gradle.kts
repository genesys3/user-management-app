plugins {
    java
    id("com.google.cloud.tools.jib") version "3.1.2"
    id("com.adarshr.test-logger") version "3.0.0"
}


configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}



dependencies {
    compileOnly(group = "org.projectlombok", name = "lombok", version = "1.18.20")
    annotationProcessor(group = "org.projectlombok", name = "lombok", version = "1.18.20")

    implementation(platform("org.springframework.boot:spring-boot-dependencies:2.5.3"))

    implementation(group = "org.springframework.boot", name = "spring-boot-starter-security")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-web")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-data-elasticsearch")
    implementation(group = "org.springframework.boot", name = "spring-boot-starter-validation")

    implementation(group = "org.springdoc", name = "springdoc-openapi-ui", version = "1.5.10")

    testImplementation(group = "org.springframework.boot", name = "spring-boot-starter-test")
    testImplementation(group = "org.springframework.security", name = "spring-security-test")
    testImplementation(group = "org.assertj", name = "assertj-core", version = "3.20.2")
    testImplementation(group = "org.mockito", name = "mockito-junit-jupiter")
    testImplementation(group = "org.mockito", name = "mockito-core")
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter-api")
    testRuntimeOnly(group = "org.junit.jupiter", name = "junit-jupiter-engine")
}

tasks.withType(Test::class) {
    useJUnitPlatform()
}


// Lets create our docker images
val mainClassName = " com.rameshkp.user.management.Application"
val entryPointScriptFile = file("$buildDir/docker/entrypoint.sh")
val classPaths = listOf(
    "/app/libs/*",
    "/app/resources",
    "/app/classes"
).joinToString(separator = ":")
val createEntryPointScript by tasks.registering {
    outputs.files(entryPointScriptFile)
    doLast {
        entryPointScriptFile.parentFile.mkdirs()
        file(entryPointScriptFile).writeText(
            """
            #!/usr/bin/env bash
            
            # wait for elastic search
            result=1
            while [[ ${'$'}result -gt 0 ]]
            do
              (echo > /dev/tcp/${'$'}ES_HOST/9200) >/dev/null 2>&1
              result=$?
              sleep 5s
              echo "Waiting for elastic connectivity ${'$'}ES_HOST/9200"
            done
            exec java -cp "$classPaths" $mainClassName
            """.trimIndent()
        )
    }
}

jib {
    from {
        image = "adoptopenjdk/openjdk15"
    }
    to {
        image = "genesys-user-management-application"
    }
    container {
        appRoot = "/app"
        workingDirectory = "/app"
        entrypoint = listOf("/app/entrypoint.sh")
    }
    extraDirectories {
        paths {
            path {
                from {
                    setFrom(entryPointScriptFile.parentFile)
                }
                into = "/app"
            }
        }
        permissions = mapOf("/app/entrypoint.sh" to "755")
    }
}

val jibDockerBuild by tasks.existing {
    dependsOn(createEntryPointScript)
}