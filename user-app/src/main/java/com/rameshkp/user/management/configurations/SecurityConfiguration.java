package com.rameshkp.user.management.configurations;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *  A configuration class to configure security for this application
 */
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * A password encoder to encode passwords
     *
     * @return A PasswordEncoder object
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Configure basic security
        http.httpBasic();
        // We will disable csrf
        http.csrf().disable();

        // Here we will authorize our requests
        http.authorizeRequests()
                .mvcMatchers("/login").authenticated() // Login method should be accessible only to authenticated users
                .anyRequest().permitAll(); // All other requests should be allowed.
    }
}
