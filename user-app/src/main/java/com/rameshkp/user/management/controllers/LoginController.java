package com.rameshkp.user.management.controllers;

import com.rameshkp.user.management.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Login API", description = "An API to manage logins")
@RestController
@RequestMapping("/login")
public class LoginController {
    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @Operation(
            summary = "An api to login",
            description = "When a user logins to this api his login time would be recorded",
            security = @SecurityRequirement(name = "basicAuth")
    )
    @GetMapping
    public ResponseEntity<String> doLogin(Authentication authentication) {
        String previousLoginData = userService.updateLastLoginAndGetPreviousValue(authentication.getName());

        String returnValue = "You have successfully logged In.";
        if (previousLoginData != null) {
            returnValue += " Your last login @ " + previousLoginData;
        }

        return new ResponseEntity<>(returnValue, HttpStatus.OK);
    }

}
