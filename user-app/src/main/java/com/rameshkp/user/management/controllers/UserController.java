package com.rameshkp.user.management.controllers;

import com.rameshkp.user.management.models.User;
import com.rameshkp.user.management.models.ValidationError;
import com.rameshkp.user.management.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 *  A controller to manage users within the application
 */
@Tag(name = "User API", description = "An api to manage Users.")
@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * A method to create a new user
     *
     * @param user the user object to create
     *
     * @return the newly created user.
     */
    @Operation(
            summary = "Create a new user.",
            description = "An api operation to create a new user.",
            responses =  {
                    @ApiResponse(responseCode = "201", description = "User was created successfully", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "User field validation failed", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ValidationError.class)))
                    }),
                    @ApiResponse(responseCode = "409", description = "User with same email already exists in the system.", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ValidationError.class)))
                    })
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "The user object to create",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = User.class
                            )
                    )
            )
    )
    @PostMapping
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        User savedUser = this.userService.createUser(user);
        log.info("User {} created successfully", savedUser);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    /**
     * A method to update a user
     *
     * Note: User can only update name, email and password
     *
     * @param userId id of the user to update
     * @param user the user object to update
     * @return the updated user.
     */
    @Operation(
            summary = "Update a user.",
            description = "An api operation to update a user.",
            responses =  {
                    @ApiResponse(responseCode = "200", description = "User was updated successfully", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
                    }),
                    @ApiResponse(responseCode = "400", description = "User field validation failed", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ValidationError.class)))
                    }),
                    @ApiResponse(responseCode = "404", description = "User with provided id not found in the system.", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ValidationError.class)))
                    })
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "The user data to update in the system.",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(
                                    implementation = User.class
                            )
                    )
            )
    )
    @PutMapping("/{userId}")
    public ResponseEntity<User> updateUser(@NotBlank @PathVariable("userId") String userId, @RequestBody User user) {
        // Set userid to object
        user.setUserId(userId);
        User updatedUser = this.userService.updateUser(user);
        log.info("User {} updated successfully", updatedUser);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    /**
     * A method to get a user with provided user id
     *
     * @param userId id of the user to get
     * @return the user object
     */
    @Operation(
            summary = "Get a user.",
            description = "An api operation to get a user.",
            responses =  {
                    @ApiResponse(responseCode = "200", description = "Retrived the user successfully", content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
                    }),
                    @ApiResponse(responseCode = "404", description = "User with provided id not found in the system.", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ValidationError.class)))
                    })
            }
    )
    @GetMapping("/{userId}")
    public ResponseEntity<User> getUser(@NotBlank @PathVariable("userId") String userId) {
        User getUser = this.userService.getUser(userId);
        log.info("User {} retrieved successfully", getUser);
        return new ResponseEntity<>(getUser, HttpStatus.OK);
    }

    /**
     * A method to delete a user from the database
     *
     * @param userId id of the user to delete from database
     */
    @Operation(
            summary = "Delete a user.",
            description = "An api operation to delete a user.",
            responses =  {
                    @ApiResponse(responseCode = "204", description = "User was deleted successfully"),
                    @ApiResponse(responseCode = "404", description = "User with provided id not found in the system.", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = ValidationError.class)))
                    })
            }
    )
    @DeleteMapping("/{userId}")
    public ResponseEntity<?> deleteUser(@NotBlank @PathVariable("userId") String userId) {
        this.userService.deleteUser(userId);
        log.info("User with id {} deleted successfully", userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    /**
     * A method to get all user in the system
     *
     * @return a collection of user objects from the system.
     */
    @Operation(
            summary = "Get all users.",
            description = "An api operation to get all users in the system.",
            responses =  {
                    @ApiResponse(responseCode = "200", description = "Retrieved all user successfully", content = {
                            @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = User.class)))
                    })
            }
    )
    @GetMapping
    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<>(this.userService.getAllUsers(), HttpStatus.OK);
    }

}
