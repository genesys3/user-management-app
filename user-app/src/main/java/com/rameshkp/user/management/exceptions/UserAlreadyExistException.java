package com.rameshkp.user.management.exceptions;

/**
 * An instance of this exception class should be thrown when user already exists in the database.
 */
public class UserAlreadyExistException extends RuntimeException {
    public UserAlreadyExistException(String message) {
        super(message);
    }
}
