package com.rameshkp.user.management.exceptions;

/**
 *  An instance of this class should be thrown when the user is not found in the database.
 */
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String message) {
        super(message);
    }
}
