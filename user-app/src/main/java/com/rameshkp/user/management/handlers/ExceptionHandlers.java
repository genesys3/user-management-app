package com.rameshkp.user.management.handlers;

import com.rameshkp.user.management.exceptions.UserAlreadyExistException;
import com.rameshkp.user.management.exceptions.UserNotFoundException;
import com.rameshkp.user.management.models.ValidationError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 *  A class to handle all exceptions
 *
 *  This helps to return a valid error message to the user
 */
@ControllerAdvice
public class ExceptionHandlers {

    /**
     * A method to handle method argument not valid exception.
     *
     * @param e MethodArgumentNotValidException object
     * @return A valid response entity
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ValidationError>> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        var fieldErrors = new ArrayList<ValidationError>();
        for (var error : e.getFieldErrors()) {
            fieldErrors.add(new ValidationError(error.getField(), error.getDefaultMessage()));
        }
        return new ResponseEntity<>(fieldErrors, HttpStatus.BAD_REQUEST);
    }

    /**
     * A method to handle constraint validation exception
     *
     * @param e ConstraintViolationException object
     * @return A valid response entity
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<List<ValidationError>> handleConstraintViolationException(ConstraintViolationException e) {
        var fieldErrors = new ArrayList<ValidationError>();
        for (var constraintViolation: e.getConstraintViolations()) {
            fieldErrors.add(new ValidationError(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
        }
        return new ResponseEntity<>(fieldErrors, HttpStatus.BAD_REQUEST);
    }

    /**
     * A method to handle user not found exception
     *
     * @param e UserNotFoundException object
     * @return A valid response entity
     */
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<List<ValidationError>> handleUserNotFoundException(UserNotFoundException e) {
        var errors = new ArrayList<ValidationError>();
        errors.add(new ValidationError("userId", e.getMessage()));
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

    /**
     * A method to handle user already exist exception
     *
     * @param e UserAlreadyExistException object
     * @return A valid response entity
     */
    @ExceptionHandler(UserAlreadyExistException.class)
    public ResponseEntity<List<ValidationError>> handleUserAlreadyExistException(UserAlreadyExistException e) {
        var errors = new ArrayList<ValidationError>();
        errors.add(new ValidationError("userId", e.getMessage()));
        return new ResponseEntity<>(errors, HttpStatus.CONFLICT);
    }

}
