package com.rameshkp.user.management.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

/**
 *  A model to represent a user
 */
@Data
@Document(indexName = "users")
public class User {
    @Id
    private String userId;
    @NotBlank(message = "Name field cannot be blank.")
    private String name;
    @Email(message = "Email field not valid")
    private String email;
    @NotBlank(message = "Password field cannot be blank.")
    @Size(min = 6, message = "Password should be at least 6 character")
    private String password;
    @Null(message = "lastLogin field is automatically updated by system. Hence set its value to null.")
    private String lastLogin;
}
