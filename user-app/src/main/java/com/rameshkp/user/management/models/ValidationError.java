package com.rameshkp.user.management.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  A class to represent user field validation error
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class ValidationError {
    /**
     *  The field for which validation failed
     */
    private String field;
    /**
     *  The reason why the validation failed.
     */
    private String message;
}
