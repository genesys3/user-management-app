package com.rameshkp.user.management.repositories;

import com.rameshkp.user.management.models.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *  A repository to store user
 */
@Repository
public interface UserRepository extends ElasticsearchRepository<User, String> {
    /**
     * Find a user by their email address
     * @param email email address of the user
     * @return an optional object of user
     */
    Optional<User> findByEmail(String email);

    List<User> findAll();
}
