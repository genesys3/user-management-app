package com.rameshkp.user.management.services;

import com.rameshkp.user.management.models.SecurityUser;
import com.rameshkp.user.management.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *  A service to use elastic search repository as a database for authentication
 */
@Service
public class ElasticUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    public ElasticUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var optionalUser = userRepository.findByEmail(username);
        var user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("User with provided email not found"));
        return new SecurityUser(user);
    }
}
