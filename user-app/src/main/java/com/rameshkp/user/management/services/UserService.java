package com.rameshkp.user.management.services;

import com.rameshkp.user.management.exceptions.UserAlreadyExistException;
import com.rameshkp.user.management.exceptions.UserNotFoundException;
import com.rameshkp.user.management.models.User;
import com.rameshkp.user.management.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

/**
 *  A service class to operate on User object
 */
@Slf4j
@Service
public class UserService {
    private final UserRepository userRepository;
    private final Validator validator;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository,
                       Validator validator,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.validator = validator;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Create a provided user and return the same
     *
     * @param user the user object to add to db
     * @return the newly created user returned from the db
     * @throws UserAlreadyExistException a runtime exception when the user with provided email already exists in the database
     */
    public User createUser(User user) {
        // A user can be created only if their emails are unique
        var optionalUser = userRepository.findByEmail(user.getEmail());
        if (optionalUser.isPresent()) {
            String errorMessage = String.format("An user with email %s already exists.", user.getEmail());
            log.error(errorMessage);
            throw new UserAlreadyExistException(errorMessage);
        }

        // Set the user id to unique UUID. We won't use the generated ID from elastic db
        user.setUserId(UUID.randomUUID().toString());
        // Last login should be null when the user is first created.
        user.setLastLogin(null);
        // Before we save our user we will encode their password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        log.debug("Saving user {} to the database", user);
        return this.userRepository.save(user);
    }

    /**
     * Get a user with provided user id
     *
     * @param userId user id of the user
     * @return the user object from db
     * @throws UserNotFoundException a runtime exception, when user not found in the database
     */
    public User getUser(String userId) {
        var optionalUser = userRepository.findById(userId);

        // return the user if found else throw an exception
        return optionalUser.orElseThrow(() -> {
            String errorMessage = String.format("User with userId %s not found.", userId);
            log.error(errorMessage);
            throw new UserNotFoundException(errorMessage);
        });
    }

    /**
     * Delete a user with provided userId from the database
     *
     * @param userId user id of the user
     * @throws UserNotFoundException a runtime exception, when user not found in the database
     */
    public void deleteUser(String userId) {
        var user = getUser(userId);
        log.debug("Deleting user {} from the db", user);
        userRepository.deleteById(user.getUserId());
    }

    /**
     * Update the details of the provided user.
     *
     *  Note: User can only update name, email and password
     * @param user the saved user object from database
     */
    public User updateUser(User user) {
        var userFromDb = getUser(user.getUserId());
        // Is user trying to update his password. We assume user is always trying to update his password
        // This field is to make sure we can later encode the password correctly
        boolean isUpdatePassword = true;

        // If the user is not updating the name then set the default value
        if (user.getName() == null) {
            log.debug("Setting user name property from the database");
            user.setName(userFromDb.getName());
        }
        // If the user is not updating the password then set the default value
        if (user.getPassword() == null) {
            log.debug("Setting user password property from the database");
            user.setPassword(userFromDb.getPassword());
            isUpdatePassword = false;
        }

        // If the user is not updating the email then set the default value
        if (user.getEmail() == null) {
            log.debug("Setting user email property from the database");
            user.setEmail(userFromDb.getEmail());
        }

        // Let's validate to make sure user has provided valid values for all field
        var constraintViolations = validator.validate(user);
        if (!constraintViolations.isEmpty()) {
            log.error("Constraint violations {} in user object. User update aborted.", constraintViolations);
            throw new ConstraintViolationException(constraintViolations);
        }

        // Make sure to update the last login details from the database
        user.setLastLogin(userFromDb.getLastLogin());
        // Let's make sure to encode our password
        if (isUpdatePassword) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        log.debug("Updating user {} to the database", user);
        return userRepository.save(user);
    }


    /**
     * Get all the users from the database
     *
     * @return A iterable collection of user objects
     */
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    /**
     * This method updates the lastLogin value for the user.
     * The method also returns the previous value of the lastLogin field.
     *
     * @param email of the user for which last login needs to be updated
     * @return the previous value of the lastLogin Field
     */
    public String updateLastLoginAndGetPreviousValue(String email) {
        var optionalUser = userRepository.findByEmail(email);

        var user = optionalUser.orElseThrow(() -> {
            log.error("User with email {} not found in the system.", email);
            throw new UserNotFoundException("User with provide email not found in the system. " + email);
        });

        // Get the previous values
        String lastLoginPreviousValue = user.getLastLogin();

        // Set the last login values
        user.setLastLogin(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss")));

        var savedUser = userRepository.save(user);
        log.debug("Updated lastLogin value for the user {}", savedUser);

        return lastLoginPreviousValue;
    }

}
