package com.rameshkp.user.management.controllers;

import com.rameshkp.user.management.models.User;
import com.rameshkp.user.management.repositories.UserRepository;
import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LoginControllerTest {
    @MockBean
    private UserRepository userRepositoryMock;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String baseUrl;

    private final User validUser = new User();

    @BeforeAll
    void init() {
        baseUrl = "http://localhost:" + port + "/login";

        // A valid user data
        validUser.setUserId("abcd-1234");
        validUser.setName("test");
        // Becrypt encoded value for password = password
        validUser.setPassword("$2a$10$1b0nNey21HbNAiyO/iVW2u8B9ekt5Uf7zVuLDgSSDTPfxgXpKFUhO");
        validUser.setEmail("valid@email.com");
        validUser.setLastLogin(null);
    }

    @Test
    void when_not_authenticated_should_not_be_allowed_to_access_login_page() {
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.empty());

        var response = restTemplate.exchange(baseUrl, HttpMethod.GET, new HttpEntity<>(""), String.class);
        assertNotNull(response);
        assertEquals(401, response.getStatusCodeValue());
    }


    @Test
    void when_valid_authentication_provided_and_user_logins_for_first_time_login_succeeds() {
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.of(validUser));

        HttpHeaders header = new HttpHeaders();
        header.add("Authorization", getBasicAuthHeaderValue("valid@email.com", "password"));
        var response = restTemplate.exchange(baseUrl, HttpMethod.GET, new HttpEntity<>(header), String.class);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("You have successfully logged In.", response.getBody());
    }

    @Test
    void when_valid_authentication_provided_and_user_logins_for_second_time_should_return_last_login_value() {
        validUser.setLastLogin("22-Aug-2021 10:35:55");
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.of(validUser));

        HttpHeaders header = new HttpHeaders();
        header.add("Authorization", getBasicAuthHeaderValue("valid@email.com", "password"));
        var response = restTemplate.exchange(baseUrl, HttpMethod.GET, new HttpEntity<>(header), String.class);

        assertEquals(200, response.getStatusCodeValue());
        assertEquals("You have successfully logged In. Your last login @ 22-Aug-2021 10:35:55", response.getBody());
    }


    private String getBasicAuthHeaderValue(String userName, String password) {
        String auth = userName + ":" + password;
        return "Basic " + Base64.encodeBase64String(auth.getBytes(StandardCharsets.UTF_8));
    }

}