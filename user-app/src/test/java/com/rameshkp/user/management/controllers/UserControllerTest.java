package com.rameshkp.user.management.controllers;

import com.rameshkp.user.management.models.User;
import com.rameshkp.user.management.models.ValidationError;
import com.rameshkp.user.management.repositories.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {
    @MockBean
    private UserRepository userRepositoryMock;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String baseUrl;

    private final User validUser = new User();
    private final User invalidUser = new User();

    @BeforeAll
    void init() {
        baseUrl = "http://localhost:" + port + "/users";

        // An invalid user data
        invalidUser.setName(" ");
        invalidUser.setPassword("p");
        invalidUser.setEmail("invalid");
        invalidUser.setLastLogin("some value");

        // A valid user data
        validUser.setName("test");
        validUser.setPassword("password");
        validUser.setEmail("valid@email.com");
    }

    // create user tests
    @Test
    void when_data_not_valid_user_cannot_be_created_and_should_return_error_response() {
        // when
        var response = restTemplate.postForEntity(baseUrl, invalidUser, ValidationError[].class);
        assertNotNull(response);
        assertEquals(400, response.getStatusCodeValue());
        assertEquals(4, response.getBody().length);
    }

    @Test
    void when_user_with_same_email_exist_then_user_cannot_be_created_and_should_return_error_response() {
        // when
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.of(validUser));

        var response = restTemplate.postForEntity(baseUrl, validUser, ValidationError[].class);
        assertNotNull(response);
        assertEquals(409, response.getStatusCodeValue());
        var validationError = response.getBody()[0];
        assertEquals("An user with email valid@email.com already exists.", validationError.getMessage());
    }

    @Test
    void when_all_data_valid_user_can_be_created_successfully() {
        // when
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.empty());
        when(userRepositoryMock.save(any())).thenReturn(UserControllerTest.this.validUser);

        var response = restTemplate.postForEntity(baseUrl, UserControllerTest.this.validUser, User.class);
        assertNotNull(response);
        assertEquals(201, response.getStatusCodeValue());
    }

    // Update the user
    @Test
    void when_update_user_and_user_not_found_should_return_error_response() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.empty());

        HttpEntity<User> userHttpEntity = new HttpEntity<>(validUser);

        var response = restTemplate.exchange(baseUrl + "/test", HttpMethod.PUT, userHttpEntity, ValidationError[].class);
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
        assertEquals("User with userId test not found.", response.getBody()[0].getMessage());
    }

    @Test
    void when_data_not_valid_user_cannot_be_updated_and_should_return_error_response() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(validUser));

        HttpEntity<User> userHttpEntity = new HttpEntity<>(invalidUser);
        var response = restTemplate.exchange(baseUrl + "/test", HttpMethod.PUT, userHttpEntity, ValidationError[].class);
        assertNotNull(response);
        assertEquals(400, response.getStatusCodeValue());
        assertEquals(4, response.getBody().length);
    }

    @Test
    void when_all_data_valid_user_can_be_updated_successfully() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(validUser));
        when(userRepositoryMock.save(any())).thenReturn(validUser);

        User userDetailsToUpdate = new User();
        userDetailsToUpdate.setPassword("newPassword");
        userDetailsToUpdate.setEmail("new@email.com");

        HttpEntity<User> userHttpEntity = new HttpEntity<>(userDetailsToUpdate);
        var response = restTemplate.exchange(baseUrl + "/test", HttpMethod.PUT, userHttpEntity, User.class);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
    }

    // Delete user tests
    @Test
    void when_delete_user_and_user_not_found_should_return_a_error_response() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.empty());

        HttpEntity<User> userHttpEntity = new HttpEntity<>(validUser);
        var response = restTemplate.exchange(baseUrl + "/test", HttpMethod.DELETE, userHttpEntity, ValidationError[].class);
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
        assertEquals("User with userId test not found.", response.getBody()[0].getMessage());
    }

    @Test
    void when_delete_a_valid_user_should_delete_and_return_a_valid_response() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(validUser));
        doNothing().when(userRepositoryMock).deleteById(any());

        HttpEntity<String> userHttpEntity = new HttpEntity<>("");
        var response = restTemplate.exchange(baseUrl + "/test", HttpMethod.DELETE, userHttpEntity, String.class);
        assertNotNull(response);
        assertEquals(204, response.getStatusCodeValue());
        verify(userRepositoryMock, times(1)).deleteById(any());
    }

    // Get user tests
    @Test
    void when_get_user_and_user_not_found_should_return_a_error_response() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.empty());

        var response = restTemplate.getForEntity(baseUrl + "/test", ValidationError[].class);
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
        assertEquals("User with userId test not found.", response.getBody()[0].getMessage());
    }

    @Test
    void when_get_user_and_user_found_should_return_response_with_user_data() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(validUser));

        var response = restTemplate.getForEntity(baseUrl + "/test", User.class);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(validUser, response.getBody());
    }

    @Test
    void when_get_all_users_and_no_users_in_system_should_return_an_emtpy_iterable_object() {
        when(userRepositoryMock.findAll()).thenReturn(List.of());

        var response = restTemplate.getForEntity(baseUrl, User[].class);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(0, response.getBody().length);
    }

    @Test
    void when_get_all_users_and_should_return_all_users_in_the_system() {
        when(userRepositoryMock.findAll()).thenReturn(List.of(validUser));

        var response = restTemplate.getForEntity(baseUrl, User[].class);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(1, response.getBody().length);
    }

}