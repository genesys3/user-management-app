package com.rameshkp.user.management.handlers;

import com.rameshkp.user.management.exceptions.UserAlreadyExistException;
import com.rameshkp.user.management.exceptions.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ExceptionHandlersTest {
    private final ExceptionHandlers classUnderTest = new ExceptionHandlers();

    @Test
    void when_method_argument_not_valid_exception_should_return_a_valid_response() {
        // Given
        FieldError e1 = new FieldError("test", "f1", "f1 test");
        FieldError e2 = new FieldError("test", "f2", "f2 test");
        var mANVEMock = Mockito.mock(MethodArgumentNotValidException.class);
        when(mANVEMock.getFieldErrors()).thenReturn(List.of(e1, e2));

        // when
        var response= classUnderTest.handleMethodArgumentNotValidException(mANVEMock);
        assertNotNull(response);
        assertEquals(400, response.getStatusCodeValue());
        var body = response.getBody();
        assertEquals(2, body.size());
    }

    @Test
    void when_constraint_violation_exception_should_return_a_valid_response() {
        // Given
        var constraintViolation1 = Mockito.mock(ConstraintViolation.class);
        var constraintViolation2 = Mockito.mock(ConstraintViolation.class);
        var path1 = Mockito.mock(Path.class);
        var path2 = Mockito.mock(Path.class);
        when(constraintViolation1.getMessage()).thenReturn("password should not be null");
        when(constraintViolation1.getPropertyPath()).thenReturn(path1);
        when(path1.toString()).thenReturn("password");
        when(constraintViolation2.getMessage()).thenReturn("Name should not be blank");
        when(constraintViolation2.getPropertyPath()).thenReturn(path2);
        when(path2.toString()).thenReturn("name");

        ConstraintViolationException cve = Mockito.mock(ConstraintViolationException.class);
        when(cve.getConstraintViolations()).thenReturn(Set.of(constraintViolation1, constraintViolation2));

        var responseEntity = classUnderTest.handleConstraintViolationException(cve);
        assertNotNull(responseEntity);
        assertEquals(400, responseEntity.getStatusCodeValue());
        var body = responseEntity.getBody();
        assertEquals(2, body.size());
    }

    @Test
    void when_user_not_found_exception_should_return_a_valid_response() {
        var unfe = new UserNotFoundException("User with id test not Found.");

        var responseEntity = classUnderTest.handleUserNotFoundException(unfe);
        assertNotNull(responseEntity);
        assertEquals(404, responseEntity.getStatusCodeValue());
        var body = responseEntity.getBody();
        assertEquals(1, body.size());
        var validationError = body.get(0);
        assertEquals("User with id test not Found.", validationError.getMessage());
        assertEquals("userId", validationError.getField());
    }

    @Test
    void when_user_already_exist_exception_should_return_a_valid_response() {
        var uaee = new UserAlreadyExistException("User with id test already exits");

        var responseEntity = classUnderTest.handleUserAlreadyExistException(uaee);
        assertNotNull(responseEntity);
        assertEquals(409, responseEntity.getStatusCodeValue());
        var body = responseEntity.getBody();
        assertEquals(1, body.size());
        var validationError = body.get(0);
        assertEquals("User with id test already exits", validationError.getMessage());
        assertEquals("userId", validationError.getField());
    }

}