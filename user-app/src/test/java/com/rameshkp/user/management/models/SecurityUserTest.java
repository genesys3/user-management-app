package com.rameshkp.user.management.models;

import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SecurityUserTest {

    @Test
    void test_security_user_can_be_created() {
        // Given a user
        User user = new User();
        user.setUserId("abcd-efgh");
        user.setName("test");
        user.setPassword("password");
        user.setEmail("test@gmail.com");

        // test a security user can be created
        SecurityUser securityUser = new SecurityUser(user);
        assertEquals(user.getEmail(), securityUser.getUsername());
        assertEquals(user.getPassword(), securityUser.getPassword());
        assertTrue(securityUser.isAccountNonExpired());
        assertTrue(securityUser.isAccountNonLocked());
        assertTrue(securityUser.isEnabled());
        assertTrue(securityUser.isCredentialsNonExpired());
        assertEquals(List.of(new SimpleGrantedAuthority("user")), securityUser.getAuthorities());

    }

}