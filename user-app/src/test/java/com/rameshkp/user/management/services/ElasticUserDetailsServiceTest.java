package com.rameshkp.user.management.services;

import com.rameshkp.user.management.models.User;
import com.rameshkp.user.management.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ElasticUserDetailsServiceTest {
    private final UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);
    private final ElasticUserDetailsService classUnderTest = new ElasticUserDetailsService(userRepositoryMock);

    @Test
    void when_user_found_then_loadUserByUsername_should_return_a_security_user() {
        // Given
        User user = new User();
        user.setUserId("abcd-efgh-ijkl");
        user.setPassword("password");
        user.setName("test");
        user.setEmail("test@gmail.com");
        when(userRepositoryMock.findByEmail("test@gmail.com")).thenReturn(Optional.of(user));

        // when
        var securityUser = classUnderTest.loadUserByUsername("test@gmail.com");
        assertEquals(user.getEmail(), securityUser.getUsername());
        assertEquals(user.getPassword(), securityUser.getPassword());
        assertEquals(List.of(new SimpleGrantedAuthority("user")), securityUser.getAuthorities());
        assertTrue(securityUser.isAccountNonExpired());
        assertTrue(securityUser.isAccountNonLocked());
        assertTrue(securityUser.isEnabled());
        assertTrue(securityUser.isCredentialsNonExpired());
    }

    @Test
    void when_user_not_found_then_loadUserByUsername_should_return_a_exception() {
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.empty());

        UsernameNotFoundException e = assertThrows(UsernameNotFoundException.class, () -> classUnderTest.loadUserByUsername("test@gmail.com"));
        assertNotNull(e);
        assertEquals("User with provided email not found", e.getMessage());
    }
}