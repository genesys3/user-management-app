package com.rameshkp.user.management.services;

import com.rameshkp.user.management.exceptions.UserAlreadyExistException;
import com.rameshkp.user.management.exceptions.UserNotFoundException;
import com.rameshkp.user.management.models.User;
import com.rameshkp.user.management.repositories.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {
    private final User user = new User();
    private final UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private final UserService classUnderTest = new UserService(userRepositoryMock, validator, passwordEncoder);

    @BeforeAll
    void init() {
        user.setUserId("abcd-1234");
        user.setEmail("test@gmail.com");
        user.setName("test");
        user.setPassword("password");
    }

    // Creation test cases
    @Test
    void when_create_user_and_user_already_exist_should_throw_exception() {
        // Given
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.of(user));

        UserAlreadyExistException e = assertThrows(UserAlreadyExistException.class, () -> classUnderTest.createUser(user));
        assertEquals("An user with email test@gmail.com already exists.", e.getMessage());
    }

    @Test
    void when_create_user_and_user_not_found_should_create_user() {
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.empty());
        when(userRepositoryMock.save(any())).thenReturn(user);

        User savedUser = classUnderTest.createUser(user);
        assertNotNull(savedUser);
        verify(userRepositoryMock, times(1)).save(user);
    }

    // List user test cases
    @Test
    void when_get_user_and_user_not_found_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.empty());

        UserNotFoundException e = assertThrows(UserNotFoundException.class, () -> classUnderTest.getUser("test"));
        assertEquals("User with userId test not found.", e.getMessage());
    }

    @Test
    void when_get_user_and_user_found_should_return_the_user() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        User getUser = classUnderTest.getUser("abcd-1234");
        assertNotNull(getUser);
        assertEquals(user, getUser);
    }

    // Delete user test cases
    @Test
    void when_delete_user_and_user_not_found_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.empty());

        UserNotFoundException e = assertThrows(UserNotFoundException.class, () -> classUnderTest.deleteUser("test"));
        assertEquals("User with userId test not found.", e.getMessage());
    }

    @Test
    void when_delete_user_and_user_found_should_return_the_user() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));
        doNothing().when(userRepositoryMock).deleteById(any());

        classUnderTest.deleteUser("test");
        verify(userRepositoryMock, times(1)).deleteById(any());
    }

    // Update user test cases
    @Test
    void when_update_user_and_user_not_found_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.empty());

        user.setUserId("test");
        UserNotFoundException e = assertThrows(UserNotFoundException.class, () -> classUnderTest.updateUser(user));
        assertEquals("User with userId test not found.", e.getMessage());
    }

    @Test
    void when_update_user_with_blank_name_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        User updateUser = new User();
        updateUser.setName("  ");

        ConstraintViolationException e = assertThrows(ConstraintViolationException.class, () -> classUnderTest.updateUser(updateUser));
        assertEquals(1, e.getConstraintViolations().size());
        assertEquals("Name field cannot be blank.", e.getConstraintViolations().stream().findFirst().get().getMessage());
    }

    @Test
    void when_update_user_with_invalid_password_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        User updateUser = new User();
        updateUser.setPassword("123");

        ConstraintViolationException e = assertThrows(ConstraintViolationException.class, () -> classUnderTest.updateUser(updateUser));
        assertEquals(1, e.getConstraintViolations().size());
        assertEquals("Password should be at least 6 character", e.getConstraintViolations().stream().findFirst().get().getMessage());
    }

    @Test
    void when_update_user_with_invalid_email_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        User updateUser = new User();
        updateUser.setEmail("test");

        ConstraintViolationException e = assertThrows(ConstraintViolationException.class, () -> classUnderTest.updateUser(updateUser));
        assertEquals(1, e.getConstraintViolations().size());
        assertEquals("Email field not valid", e.getConstraintViolations().stream().findFirst().get().getMessage());
    }

    @Test
    void when_update_user_and_last_login_field_set_should_throw_exception() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        User updateUser = new User();
        updateUser.setLastLogin("test");

        ConstraintViolationException e = assertThrows(ConstraintViolationException.class, () -> classUnderTest.updateUser(updateUser));
        assertEquals(1, e.getConstraintViolations().size());
        assertEquals("lastLogin field is automatically updated by system. Hence set its value to null.", e.getConstraintViolations().stream().findFirst().get().getMessage());
    }

    @Test
    void when_update_user_with_multiple_invalid_values_exception_should_report_them_all() {
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        User updateUser = new User();
        updateUser.setEmail("test");
        updateUser.setPassword("123");
        updateUser.setName("  ");

        ConstraintViolationException e = assertThrows(ConstraintViolationException.class, () -> classUnderTest.updateUser(updateUser));
        assertEquals(3, e.getConstraintViolations().size());
    }

    @Test
    void when_update_user_with_valid_name_only_name_should_be_updated() {
        //Given a user in db
        user.setUserId("abcd-efgh");
        user.setName("somename");
        user.setPassword("123456");
        user.setEmail("test@gmail.com");
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        // Let's try and update the name
        User updateUser = new User();
        updateUser.setName("test");
        when(userRepositoryMock.save(any())).thenReturn(updateUser);

        User savedUser = classUnderTest.updateUser(updateUser);

        // Make sure our name was updated
        assertEquals("test", savedUser.getName());
        // Make sure all other values stays the default
        assertEquals("123456", savedUser.getPassword());
        assertEquals("test@gmail.com", savedUser.getEmail());
    }

    @Test
    void when_update_user_with_valid_password_only_password_should_be_updated() {
        //Given a user in db
        user.setUserId("abcd-efgh");
        user.setName("test");
        user.setPassword("123456");
        user.setEmail("test@gmail.com");
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        // Let's try and update the name
        User updateUser = new User();
        updateUser.setPassword("password");
        when(userRepositoryMock.save(any())).thenReturn(updateUser);

        User savedUser = classUnderTest.updateUser(updateUser);

        // Make sure our password was updated
        assertTrue(passwordEncoder.matches("password", savedUser.getPassword()));
        // Make sure all other values stays the default
        assertEquals("test", savedUser.getName());
        assertEquals("test@gmail.com", savedUser.getEmail());
    }

    @Test
    void when_update_user_with_valid_email_only_email_should_be_updated() {
        //Given a user in db
        user.setUserId("abcd-efgh");
        user.setName("test");
        user.setPassword(passwordEncoder.encode("password"));
        user.setEmail("test@gmail.com");
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        // Let's try and update the name
        User updateUser = new User();
        updateUser.setEmail("thisisnew@gmail.com");
        when(userRepositoryMock.save(any())).thenReturn(updateUser);

        User savedUser = classUnderTest.updateUser(updateUser);

        // Make sure our email was updated
        assertEquals("thisisnew@gmail.com", savedUser.getEmail());
        // Make sure all other values stays the default
        assertEquals("test", savedUser.getName());
        assertTrue(passwordEncoder.matches("password", savedUser.getPassword()));
    }

    @Test
    void when_update_user_with_multiple_valid_values_all_values_should_be_updated() {
        //Given a user in db
        user.setUserId("abcd-efgh");
        user.setName("test");
        user.setPassword("password");
        user.setEmail("test@gmail.com");
        when(userRepositoryMock.findById(any())).thenReturn(Optional.of(user));

        // Let's try and update the name
        User updateUser = new User();
        updateUser.setName("t1");
        updateUser.setPassword("testpass");
        updateUser.setEmail("updated@gmail.com");
        when(userRepositoryMock.save(any())).thenReturn(updateUser);

        User savedUser = classUnderTest.updateUser(updateUser);

        // Make sure all values are updated
        assertEquals("updated@gmail.com", savedUser.getEmail());
        assertEquals("t1", savedUser.getName());
        assertTrue(passwordEncoder.matches("testpass", savedUser.getPassword()));
    }

    @Test
    void when_get_all_user_and_no_user_found_should_return_an_empty_iterable_object() {
        when(userRepositoryMock.findAll()).thenReturn(List.of());

        var users  = classUnderTest.getAllUsers();
        assertNotNull(users);
        assertEquals(0, StreamSupport.stream(users.spliterator(), false).count());
    }

    @Test
    void when_get_all_user_should_return_all_users_from_the_database() {
        when(userRepositoryMock.findAll()).thenReturn(List.of(Mockito.mock(User.class), Mockito.mock(User.class)));

        var users = classUnderTest.getAllUsers();
        assertNotNull(users);
        assertEquals(2, StreamSupport.stream(users.spliterator(), false).count());
    }

    @Test
    void when_update_last_login_and_user_not_found_should_throw_exception() {
        when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.empty());

        UserNotFoundException e = assertThrows(UserNotFoundException.class, () -> classUnderTest.updateLastLoginAndGetPreviousValue("test@gmail.com"));
        assertEquals("User with provide email not found in the system. test@gmail.com", e.getMessage());
    }

    @Test
    void when_update_last_login_and_user_found_should_updated_last_login_and_return_previous_value() {
       user.setLastLogin("22-07-2021 10.30AM");

       when(userRepositoryMock.findByEmail(any())).thenReturn(Optional.of(user));
       when(userRepositoryMock.save(any())).thenReturn(user);
       String lastLogin = classUnderTest.updateLastLoginAndGetPreviousValue("test@gmail.com");
       assertEquals("22-07-2021 10.30AM", lastLogin);
       assertNotEquals("22-07-2021 10.30AM", user.getLastLogin());
    }

}