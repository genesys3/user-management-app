plugins {
    `java-base`
    id("com.avast.gradle.docker-compose") version "0.14.5"
}


dockerCompose {
    dockerComposeWorkingDirectory = "$projectDir/src/main/compose"
    useComposeFiles = listOf("docker-compose.yaml")
}


sourceSets {
    create("compose") {
        java {
            setSrcDirs(listOf("src/main/compose"))
        }
    }
}

tasks.named("composeUp") {
    dependsOn(project(":user-app").tasks.named("jibDockerBuild"))
}