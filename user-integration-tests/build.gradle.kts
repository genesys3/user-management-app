plugins {
    java
    id("com.adarshr.test-logger") version "3.0.0"
}

dependencies {
    compileOnly(group = "org.projectlombok", name = "lombok", version = "1.18.20")
    annotationProcessor(group = "org.projectlombok", name = "lombok", version = "1.18.20")

    testImplementation(platform("org.springframework.boot:spring-boot-dependencies:2.5.3"))

    testImplementation(group = "org.springframework.boot", name = "spring-boot-starter-web")
    testImplementation(group = "org.springframework.boot", name = "spring-boot-starter-test")
    testImplementation(group = "org.springframework.security", name = "spring-security-test")
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter-api")
    testRuntimeOnly(group = "org.junit.jupiter", name = "junit-jupiter-engine")
}

tasks.withType(Test::class) {
    useJUnitPlatform()
}
