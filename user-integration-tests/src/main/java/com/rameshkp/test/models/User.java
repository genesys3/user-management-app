package com.rameshkp.test.models;

import lombok.Data;

/**
 *  A model to represent a user
 */
@Data
public class User {
    private String userId;
    private String name;
    private String email;
    private String password;
    private String lastLogin;
}
