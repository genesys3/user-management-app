package com.rameshkp.user.management;

import com.rameshkp.test.models.User;
import com.rameshkp.test.models.ValidationError;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserAppIntegrationTests {
    private final String baseUrl = "http://localhost:8080";
    private final String userBaseUrl  = baseUrl + "/users";
    private final String loginBaseUrl = baseUrl + "/login";
    private final User validUser1 = new User();
    private final User validUser2 = new User();
    private final TestRestTemplate restTemplate = new TestRestTemplate();
    private final User invalidUser = new User();
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @BeforeAll
    void init() {
        // set user 1 details
        validUser1.setEmail("test-user1@gmail.com");
        validUser1.setName("test-user1");
        validUser1.setPassword("golden!Rose");

        // set user 2 details
        validUser2.setEmail("test-user2@gmail.com");
        validUser2.setName("test-user2");
        validUser2.setPassword("All!sW@ll");

        invalidUser.setPassword("pass");
        invalidUser.setEmail("invalid");
        invalidUser.setName(" ");
        invalidUser.setLastLogin("should be null");
    }

    @Order(1)
    @Test
    void when_no_user_in_the_system_get_all_users_should_return_a_empty_list() {
        var usersResponseEntity = restTemplate.getForEntity(userBaseUrl, User[].class);
        assertNotNull(usersResponseEntity);
        assertEquals(200, usersResponseEntity.getStatusCodeValue());
        assertEquals(0, usersResponseEntity.getBody().length);
    }

    @Order(2)
    @Test
    void create_a_new_user_with_invalid_data_should_return_error_response() {
        var errorResponseEntity = restTemplate.postForEntity(userBaseUrl, invalidUser, ValidationError[].class);
        assertNotNull(errorResponseEntity);
        assertEquals(400, errorResponseEntity.getStatusCodeValue());
        assertEquals(4, errorResponseEntity.getBody().length);
    }

    @Order(3)
    @Test
    void create_new_users_with_valid_data_should_create_the_user_and_return_valid_response() {
        var createUserResponseEntity = restTemplate.postForEntity(userBaseUrl, validUser1, User.class);
        assertNotNull(createUserResponseEntity);
        assertEquals(201, createUserResponseEntity.getStatusCodeValue());
        User createdUser = createUserResponseEntity.getBody();
        assertNotNull(createdUser);
        assertNotNull(createdUser.getUserId());
        assertNull(createdUser.getLastLogin());
        assertEquals(validUser1.getName(), createdUser.getName());
        assertEquals(validUser1.getEmail(), createdUser.getEmail());
        assertTrue(passwordEncoder.matches(validUser1.getPassword(), createdUser.getPassword()));

        // Set the userid to the validUser1 so that we can use it in the later stage
        validUser1.setUserId(createdUser.getUserId());
    }

    @Order(4)
    @Test
    void when_create_a_user_that_already_exists_should_return_a_error_response() {
        var errorResponseEntity = restTemplate.postForEntity(userBaseUrl, validUser1, ValidationError[].class);
        assertNotNull(errorResponseEntity);
        assertEquals(409, errorResponseEntity.getStatusCodeValue());
        assertEquals("An user with email test-user1@gmail.com already exists.", errorResponseEntity.getBody()[0].getMessage());
    }

    @Order(5)
    @Test
    void when_update_a_user_and_user_not_found_should_return_a_error_response() {
        var errorResponseEntity = restTemplate.exchange(userBaseUrl + "/test", HttpMethod.PUT, new HttpEntity<>(validUser2), ValidationError[].class);
        assertNotNull(errorResponseEntity);
        assertEquals(404, errorResponseEntity.getStatusCodeValue());
        assertEquals("User with userId test not found.", errorResponseEntity.getBody()[0].getMessage());
    }

    @Order(6)
    @Test
    void when_update_a_user_with_invalid_data_should_return_a_error_response() {
        var errorResponseEntity = restTemplate.exchange(userBaseUrl + "/" + validUser1.getUserId(), HttpMethod.PUT, new HttpEntity<>(invalidUser), ValidationError[].class);
        assertNotNull(errorResponseEntity);
        assertEquals(400, errorResponseEntity.getStatusCodeValue());
        assertEquals(4, errorResponseEntity.getBody().length);
    }

    @Order(7)
    @Test
    void when_update_a_user_with_valid_data_then_should_update_the_user_and_return_a_response() {
        var updateResponseEntity = restTemplate.exchange(userBaseUrl + "/" + validUser1.getUserId(), HttpMethod.PUT, new HttpEntity<>(validUser2), User.class);
        assertNotNull(updateResponseEntity);
        assertEquals(200, updateResponseEntity.getStatusCodeValue());
        User updatedUser = updateResponseEntity.getBody();
        assertNotNull(updatedUser);
        assertEquals(validUser1.getUserId(), updatedUser.getUserId());
        assertEquals(validUser2.getName(), updatedUser.getName());
        assertEquals(validUser2.getEmail(), updatedUser.getEmail());
        assertTrue(passwordEncoder.matches(validUser2.getPassword(), updatedUser.getPassword()));

        // set the validUser2 userid (This is because we just updated user1 with user2 details)
        validUser2.setUserId(updatedUser.getUserId());
    }

    @Order(8)
    @Test
    void when_get_a_user_and_user_not_found_should_return_a_error_response() {
        var errorResponseEntity = restTemplate.getForEntity(userBaseUrl + "/test", ValidationError[].class);
        assertNotNull(errorResponseEntity);
        assertEquals(404, errorResponseEntity.getStatusCodeValue());
        assertEquals("User with userId test not found.", errorResponseEntity.getBody()[0].getMessage());
    }

    @Order(9)
    @Test
    void when_get_a_user_and_user_found_should_return_that_user() {
        var getResponseEntity = restTemplate.getForEntity(userBaseUrl + "/" + validUser2.getUserId(), User.class);
        assertNotNull(getResponseEntity);
        assertEquals(200, getResponseEntity.getStatusCodeValue());
        var getUser = getResponseEntity.getBody();
        assertEquals(validUser2.getUserId(), getUser.getUserId());
        assertEquals(validUser2.getName(), getUser.getName());
        assertEquals(validUser2.getEmail(), getUser.getEmail());
        assertTrue(passwordEncoder.matches(validUser2.getPassword(), getUser.getPassword()));
    }

    @Order(10)
    @Test
    void when_access_login_url_without_authentication_should_return_error() {
        var errorResponse = restTemplate.exchange(loginBaseUrl, HttpMethod.GET, new HttpEntity<>(""), String.class);
        assertNotNull(errorResponse);
        assertEquals(401, errorResponse.getStatusCodeValue());
    }

    @Order(11)
    @Test
    void when_user_with_invalid_credentials_access_login_url_should_return_error() {
        var authHeader = getBasicAuthHeader(validUser2.getEmail(), "invalid");
        var errorResponse = restTemplate.exchange(loginBaseUrl, HttpMethod.GET, new HttpEntity<>(authHeader), String.class);
        assertNotNull(errorResponse);
        assertEquals(401, errorResponse.getStatusCodeValue());
    }

    @Order(12)
    @Test
    void when_user_with_valid_credentials_should_be_able_to_access_login_url() {
        var authHeader = getBasicAuthHeader(validUser2.getEmail(), validUser2.getPassword());
        var loginResponse = restTemplate.exchange(loginBaseUrl, HttpMethod.GET, new HttpEntity<>(authHeader), String.class);
        assertNotNull(loginResponse);
        assertEquals(200, loginResponse.getStatusCodeValue());
        assertEquals("You have successfully logged In.", loginResponse.getBody());
    }

    @Order(13)
    @Test
    void when_user_accesses_the_login_url_multiple_times_last_login_field_should_be_updated() {
        // User successful login attempt 1
        var authHeader = getBasicAuthHeader(validUser2.getEmail(), validUser2.getPassword());
        var loginResponse = restTemplate.exchange(loginBaseUrl, HttpMethod.GET, new HttpEntity<>(authHeader), String.class);
        assertNotNull(loginResponse);
        assertEquals(200, loginResponse.getStatusCodeValue());

        // Make sure last login field is updated
        var getResponseEntity = restTemplate.getForEntity(userBaseUrl + "/" + validUser2.getUserId(), User.class);
        var getUser1 = getResponseEntity.getBody();
        assertNotNull(getUser1);
        var previousValueForLastLogin =  getUser1.getLastLogin();
        assertNotNull(previousValueForLastLogin);

        // Let the user login again attempt 2. This time lets check previous value for lastLogin is returned correctly
        var loginResponse2 = restTemplate.exchange(loginBaseUrl, HttpMethod.GET, new HttpEntity<>(authHeader), String.class);
        assertNotNull(loginResponse2);
        assertEquals(200, loginResponse2.getStatusCodeValue());
        assertEquals("You have successfully logged In. Your last login @ " + previousValueForLastLogin, loginResponse2.getBody());
    }

    @Test
    @Order(14)
    void when_delete_a_user_and_user_not_found_should_return_a_error_response() {
        var errorResponse = restTemplate.exchange(userBaseUrl + "/test", HttpMethod.DELETE, new HttpEntity<>(""), ValidationError[].class);
        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.getStatusCodeValue());
        assertEquals(1, errorResponse.getBody().length);
        assertEquals("User with userId test not found.", errorResponse.getBody()[0].getMessage());
    }

    @Test
    @Order(15)
    void when_delete_a_valid_user_then_should_delete_the_user_and_return_a_valid_response() {
        var deleteResponse = restTemplate.exchange(userBaseUrl + "/" + validUser2.getUserId(), HttpMethod.DELETE, new HttpEntity<>(""), String.class);
        assertNotNull(deleteResponse);
        assertEquals(204, deleteResponse.getStatusCodeValue());

        // Make sure the user was deleted
        var listUserResponseError = restTemplate.getForEntity(userBaseUrl + "/" + validUser2.getUserId(), ValidationError[].class);
        assertNotNull(listUserResponseError);
        assertEquals(404, listUserResponseError.getStatusCodeValue());
    }

    @Order(16)
    @Test
    void when_test_complete_the_system_should_contain_no_users() {
        var usersResponseEntity = restTemplate.getForEntity(userBaseUrl, User[].class);
        assertNotNull(usersResponseEntity);
        assertEquals(200, usersResponseEntity.getStatusCodeValue());
        assertEquals(0, usersResponseEntity.getBody().length);
    }

    private HttpHeaders getBasicAuthHeader(String username, String password) {
        String auth = username + ":" + password;
        String encodedAuth = "Basic " + Base64.encodeBase64String(auth.getBytes(StandardCharsets.UTF_8));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", encodedAuth);
        return httpHeaders;
    }
}
